﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sumalvl1.aspx.cs" Inherits="LasMatesSonDivertidas.sumalvl1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>NIVEL 1</h1>
            <h2>Antes de jugar veamos como hacer sumas con números de 1 cifra</h2>
            <h3>Sumar dos números o cosas es juntarlos en un mismo grupo, para saber cuantos hay en total.</h3>
            <h4>Vamos a ver un ejemplo para que lo podais entender mejor:</h4>
            <br />
            <div>
                Como vemos en la figura, tenemos una cesta con 3 manzanas y otra cesta con 2 manzanas, si cogemos las manzanas de ambas cestas y las metemos todas en una cesta 
                ¿cuantas manzanas habrá en la cesta? 3 + 2 = 5, eso es tendremos 5 manzanas, veís a que ha sido fácil pues ahora os toca a vosotros. 
                <br />
                <img src="Content/ejemplo_suma.png" class="example"/>
                <br />
                <br /> Cada elemento de la suma se denomina <strong>sumando</strong>, como veis hay dos cajas que pone "SUMANDO 1" y "SUMANDO 2" 
                <br /> Pedidle a vuestro papá, mamá, profesor / profesora o herman@ que os digan dos números de una cifra, poned uno de los números en la caja de "SUMANDO 1" y el otro en la caja de "SUMANDO 2".
                <br /> Ahora viene la parte divertida, ver cuanto suman los numeros de cada sumando, haced la operación podeis ayudaros de los dedos de la mano o hacerlo de memoria, para hacer la cuenta de memoria teneis que empezar contando el primer sumando 
                (1, 2, 3, así hasta llegar al número elegido, en nuestro caso será <strong>3</strong>), lo memorizamos y seguimos contando a partir de "3" el segundo sumando (en nuestro caso <strong>2</strong>), sería contar dos más (4 y 5), y... tachán ya tenemos el resultado
                <strong>!!5¡¡</strong>
            </div>
            <div>
                <h2>¿¿Listos para jugar??</h2>
                <h3>Pídele a papá, a mamá o a tu profe que te diga dos números a sumar</h3>
            </div>
          
            <div  runat="server">
                <h3>INTRODUCE EL PRIMER SUMANDO:</h3>
                <asp:TextBox ID="sumando1" runat="server" placeholder="SUMANDO 1" ></asp:TextBox>
                <h3>INTRODUCE EL SEGUNDO SUMANDO:</h3>
                <asp:TextBox ID="sumando2" runat="server" placeholder="SUMANDO 2" ></asp:TextBox>
                <h3>INTRODUCE EL RESULTADO DE LA SUMA:</h3>
                <asp:TextBox ID="resultado" runat="server" placeholder="RESULTADO" ></asp:TextBox>
                <asp:Button ID="checklvl1" OnClick="Check" Text="COMPROBAR" runat="server" />
                <br />
                <asp:Label ID="correct" runat="server" Text="CORRECTO" Font-Bold="true" ForeColor="Green"></asp:Label>
                <br />
                <asp:Label ID="fail" runat="server" Text="INCORRECTO, PIENSALO BIEN" Font-Bold="true" ForeColor="Red"></asp:Label>
                <br />
                <asp:Button ID="playLvl1Again" OnClick="PlayLvl1Again" Text="JUGAR OTRA VEZ" runat="server" />
    </div>

    </div>
</asp:Content>
