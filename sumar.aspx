﻿<%@ Page Title="Sumar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sumar.aspx.cs" Inherits="LasMatesSonDivertidas.sumar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>APRENDE A SUMAR</h1>
        <h2>Aquí aprenderemos a sumar</h2>
        <p class="lead">
            Con esta aplicación verás que las matemáticas son entretenidas, y aprenderemos juntos las operaciones básicas.
        </p>
        <h3>Tenemos 3 niveles de dificultad</h3>
        <ul>
            <li><a href="sumalvl1.aspx">NIVEL 1</a> <br />En en nivel 1 aprenderemos a sumar con números de una cifra</li>
            <li><a href="#">NIVEL 2</a> <br />En el nivel 2 aprenderemos a sumar con números de dos cifras</li>
            <li><a href="#">NIVEL 3</a> <br />En el nivel 3 realizaremos sumas con números de hasta 3 cifras</li>
        </ul>
      
        
        
    </div>
</asp:Content>
