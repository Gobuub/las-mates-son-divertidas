﻿<%@ Page Title="Dividir" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="dividir.aspx.cs" Inherits="LasMatesSonDivertidas.dividir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>APRENDE A DIVIDIR</h1>
        <h2>Bienvenido a las mates son divertidas</h2>
        <p class="lead">
            Con esta aplicación verás que las matemáticas son entretenidas, y aprenderemos juntos las operaciones básicas.
        </p>
        <p>
            En la barra de menú encontrás los links que te llevarán a las diferentes páginas de operaciones, con nosotros aprenderás a Sumar, Restar, Multiplicar y Dividir.
        </p>
    </div>
</asp:Content>
