﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LasMatesSonDivertidas
{
    public partial class sumalvl1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            correct.Visible = false;
            fail.Visible = false;
        }

        protected void Check(object sender, EventArgs e)
        {
            int Sumando1 = Convert.ToInt32(sumando1.Text);
            int Sumando2 = Convert.ToInt32(sumando2.Text);
            int Resultado = Convert.ToInt32(resultado.Text);
           

            int Correccion = Sumando1 + Sumando2;

            if (Resultado == Correccion)
            {
                correct.Visible = true;
            }
            else
            {
                fail.Visible = true;
            }


        }
        protected void PlayLvl1Again(object sender, EventArgs e)
        {

            sumando1.Text = string.Empty;
            sumando2.Text = string.Empty;
            resultado.Text = string.Empty;

        }
    }
}